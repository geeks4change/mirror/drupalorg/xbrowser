<?php
/**
 * @class
 * Class file.
 */

interface XbrowserControllerInterface {
  static function getTypes();
  public function __construct($table);
  public function getPlugin();
  public function getType();
  public function getObjects();
  public function getObject($name);
  public function getExports($current_object);
  public function canResave();
  public function doResave($object);
}