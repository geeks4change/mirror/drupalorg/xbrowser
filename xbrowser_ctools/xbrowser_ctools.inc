<?php

/**
 * Extended ctools_get_default_object() until this patch is in: https://www.drupal.org/node/2338369
 */
function xbrowser_ctools_get_default_object($table, $name, $providing_module = '') {
  ctools_include('export');
  $schema = ctools_export_get_schema($table);
  $export = $schema['export'];

  if (!$export['default hook']) {
    return;
  }

  // Wo do not use persistent cache for module specific default.
  $use_persistent_cache = !empty($export['cache defaults']) && !$providing_module;
  // Try to load individually from cache if this cache is enabled.
  if ($use_persistent_cache) {
    $defaults = _ctools_export_get_some_defaults($table, $export, array($name));
  }
  else {
    $defaults = _xbrowser_ctools_export_get_defaults($table, $export, $providing_module); //!
  }

  $status = variable_get($export['status'], array());

  if (!isset($defaults[$name])) {
    return;
  }

  $object = $defaults[$name];

  // Determine if default object is enabled or disabled.
  if (isset($status[$object->{$export['key']}])) {
    $object->disabled = $status[$object->{$export['key']}];
  }

  $object->{$export['export type string']} = t('Default');
  $object->export_type = EXPORT_IN_CODE;
  $object->in_code_only = TRUE;

  return $object;
}

function xbrowser_ctools_get_default_modules($table) {
  ctools_include('export');
  $schema = ctools_export_get_schema($table);
  $export = $schema['export'];

  if (!$export['default hook']) {
    return;
  }

  $cache = &drupal_static('_xbrowser_ctools_export_get_defaults', array()); //!
  if (!isset($cache[$table])) {
    // Use dummy module to force building static cache.
    _xbrowser_ctools_export_get_defaults($table, $export, 'dummy'); //!
  }
  $by_module = $cache[$table];
  unset($by_module['']);
  return array_keys($by_module);
}

/**
 * Extended _ctools_export_get_defaults()with module selection.
 */
function _xbrowser_ctools_export_get_defaults($table, $export, $providing_module = '') {
  ctools_include('export');
  $cache = &drupal_static(__FUNCTION__, array());
  // Wo do not use persistent cache for module specific default.
  $use_persistent_cache = !empty($export['cache defaults']) && !$providing_module;

  // If defaults may be cached, first see if we can load from cache.
  if ($use_persistent_cache && !isset($cache[$table][''])) {
    $cache[$table][''] = _ctools_export_get_defaults_from_cache($table, $export);
  }

  if (!isset($cache[$table][''])) {
    // If we're caching, attempt to get a lock. We will wait a short time
    // on the lock, but not too long, because it's better to just rebuild
    // and throw away results than wait too long on a lock.
    if ($use_persistent_cache) {
      for ($counter = 0; !($lock = lock_acquire('ctools_export:' . $table)) && $counter > 5; $counter++) {
        lock_wait('ctools_export:' . $table, 1);
        ++$counter;
      }
    }

    $cache[$table][''] = array();

    if ($export['default hook']) {
      if (!empty($export['api'])) {
        ctools_include('plugins');
        $info = ctools_plugin_api_include($export['api']['owner'], $export['api']['api'],
          $export['api']['minimum_version'], $export['api']['current_version']);
        $modules = array_keys($info);
      }
      else {
        $modules = module_implements($export['default hook']);
      }

      foreach ($modules as $module) {
        $function = $module . '_' . $export['default hook'];
        if (function_exists($function)) {
          foreach ((array) $function($export) as $name => $object) {
            // Record the module that provides this exportable.
            $object->export_module = $module;

            if (empty($export['api'])
              || (isset($object->api_version) &&
                version_compare($object->api_version, $export['api']['minimum_version']) >= 0 &&
                version_compare($object->api_version, $export['api']['current_version']) <= 0)
            ) {
              // Latest module wins here.
              $cache[$table][''][$name] = $object;
              // PHP uses Refs so this is cheap.
              $cache[$table][$module][$name] = $object;
            }
          }
        }
      }

      drupal_alter($export['default hook'], $cache[$table]['']);

      // If we acquired a lock earlier, cache the results and release the
      // lock.
      if (!empty($lock)) {
        // Cache the index.
        $index = array_keys($cache[$table]['']);
        cache_set('ctools_export_index:' . $table, $index, $export['default cache bin']);

        // Cache each object.
        foreach ($cache[$table][''] as $name => $object) {
          cache_set('ctools_export:' . $table . ':' . $name, $object, $export['default cache bin']);
        }
        lock_release('ctools_export:' . $table);
      }
    }
  }

  // Without $providing_module this is always an array, maybe empty.
  // But with providing module no hook (NULL) is different from empty result (array()).
  return isset($cache[$table][$providing_module]) ? $cache[$table][$providing_module] : NULL;
}
