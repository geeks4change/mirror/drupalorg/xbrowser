<?php
/**
 * @class
 * Class file.
 */

class XbrowserCtoolsController extends XbrowserControllerBase implements XbrowserControllerInterface {

  static function getTypes() {
    ctools_include('export');
    $schemas = ctools_export_get_schemas();
    ksort($schemas);
    $keys = array_keys($schemas);
    $return = array_combine($keys, $keys);
    return $return;
  }

  public function getObjects() {
    ctools_include('export');
    $objects = ctools_export_crud_load_all($this->type);
    ksort($objects);
    return $objects;
  }

  public function getPlugin() {
    return 'xbrowser_ctools';
  }

  public function getObject($name) {
    ctools_include('export');
    $object = ctools_export_crud_load($this->type, $name);
    // Some loaders have strange behavior, like @see page_manager_export_task_handler_load(). Fix that!
    if (!empty($object->in_code_only)) {
      $object->export_type = EXPORT_IN_CODE;
    }
    return $object;
  }

  protected function export($item) {
    return ctools_export_crud_export($this->type, $item);
  }

  protected function getDbExport($name) {
    module_load_include('inc', 'xbrowser_ctools');
    $current_object = $this->getObject($name);
    $in_db = $current_object->export_type & EXPORT_IN_DATABASE;
    if ($in_db) {
      $export = ctools_export_crud_export($this->type, $current_object);
      return $export;
    }
    return NULL;
  }

  protected function isDisabled($name) {
    // Whoo, is this ugly. @see ctools_export_set_status.
    $schema = ctools_export_get_schema($this->type);
    // Usually default_TYPE
    $status_array_variable = $schema['export']['status'];
    $status = variable_get($status_array_variable, array());
    $disabled = !empty($status[$name]);
    return $disabled;
  }

  protected function getModules() {
    module_load_include('inc', 'xbrowser_ctools');
    $modules = xbrowser_ctools_get_default_modules($this->type);
    return $modules;
  }

  protected function getDefault($name, $module = '') {
    $default = xbrowser_ctools_get_default_object($this->type, $name, $module);
    return $default;
  }

  protected function getAlterChanges($name) {
    $last_default = $this->getDefault($name);
    // @todo Alter tracking.
    $altered = xbrowser_ctools_get_default_object($this->type, $name);
    // If the final default differs from the last module implementation, it must have been altered.
    // Exporting is the only safe way to do the comparison.
    $altered_export = $altered ? $this->export($altered) : NULL;
    $last_export = $last_default ? $this->export($last_default) : NULL;
    $is_altered = $altered_export !== $last_export;
    if ($is_altered) {
      $export_changes = array('__alter__' => $altered_export);
      return $export_changes;
    }
    return array();
  }

  public function canResave() {
    return TRUE;
  }

  public function doResave($object) {
    // Workaround a field_group bug, @see https://www.drupal.org/node/1866842
    if ($this->type === 'field_group') {
      $object = field_group_unpack($object);
    }
    ctools_include('export');
    ctools_export_crud_save($this->type, $object);
  }

}