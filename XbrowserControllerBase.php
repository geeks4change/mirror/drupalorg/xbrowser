<?php
/**
 * @class
 * Class file.
 */

abstract class XbrowserControllerBase implements XbrowserControllerInterface {
  protected $type;
  public function __construct($type) {
    $this->type = $type;
  }
  public function getType() {
    return $this->type;
  }
  public function getExports($name) {
    $is_valid = $this->is_valid_object($name);
    if (!$is_valid) {
      return NULL;
    }

    $default_exports = array();
    $modules = $this->getModules();
    $module_list = system_list('module_enabled');
    foreach ($modules as $module) {
      $default = $this->getDefault($name, $module);
      if ($default) {
        $default_export = $this->export($default);
        // Append module weight.
        $weight = $module_list[$module]->weight;
        $default_exports["$module ($weight)"] = $default_export;
      }
    }
    $alter_exports = $this->getAlterChanges($name);

    $db_export = $this->getDbExport($name);
    $db_exports = $db_export ? array(t('DB') => $db_export) : array();

    $code_exports = $default_exports + $alter_exports;
    $exports = $code_exports + $db_exports;

    $last_code_export = $code_exports ? end($code_exports) : NULL;
    $db_changed = $db_export && ($db_export !== $last_code_export);

    $result = array(
      'exports' => $exports,
      'in_code' => count($default_exports),
      'altered' => count($alter_exports),
      'in_db' => (int)(bool)$db_export,
      'db_changed' => $db_changed,
      'disabled' => $this->isDisabled($name),
    );

    return $result;
  }
  protected function is_valid_object($name) {
    return TRUE;
  }
  protected function isDisabled($name) {
    return FALSE;
  }
  public function canResave() {
    return FALSE;
  }
  public function doResave($object) {
  }
  abstract protected function export($item);
  abstract protected function getDbExport($name);
  abstract protected function getModules();
  abstract protected function getDefault($name, $module = '');
  abstract protected function getAlterChanges($name);
}
